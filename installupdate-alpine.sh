#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

sudo apk update && sudo apk upgrade

sudo apk del xf86-video-intel

sudo apk del xf86-video-nouveau

sudo apk del xf86-video-amdgpu

sudo apk del xf86-video-ati

sudo apk del xf86-video-vesa

sudo apk del xf86-video-fbdev

sudo apk update && sudo apk upgrade

sudo apk add wireplumber-logind vulkan-tools steam-devices qt5-qtwayland qt6-qtwayland breeze-gtk 7zip xarchiver xdg-user-dirs xdg-utils flatpak xdg-desktop-portal xdg-desktop-portal-kde plasma-desktop-meta util-linux-misc dolphin konsole spectacle ark kate gwenview

sudo apk update && sudo apk upgrade

sudo apk add lang

sudo apk update && sudo apk upgrade

sudo apk fix

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

clear

echo "#################################################################"

sudo rm -rf /usr/share/applications/chromium-flags.desktop

sudo rm -rf /etc/chrony/chrony.conf

sudo rm -rf /usr/bin/mini-opti

sudo rm -rf /usr/bin/opti-kernel

sudo rm -rf /usr/bin/opti-wayfire

sudo rm -rf /usr/bin/startwayfire

sudo rm -rf /etc/sysctl.conf

sudo rm -rf /usr/bin/wayfire-tweaks

echo "#################################################################"

clear

echo "#################################################################"

sudo cp chromium-flags.desktop /usr/share/applications/

sudo cp chrony.conf /etc/chrony/

sudo cp mini-opti /usr/bin/

sudo cp opti-kernel /usr/bin/

sudo cp opti-wayfire /usr/bin/

sudo cp startwayfire /usr/bin/

sudo cp sysctl.conf /etc/

sudo cp wayfire-tweaks /usr/bin/

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /usr/bin/mini-opti

sudo chmod +x /usr/bin/opti-kernel

sudo chmod +x /usr/bin/opti-wayfire

sudo chmod +x /usr/bin/startwayfire

sudo chmod +x /usr/bin/wayfire-tweaks

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(SYSCTL)"
echo "#################################################################"
cat /etc/sysctl.conf